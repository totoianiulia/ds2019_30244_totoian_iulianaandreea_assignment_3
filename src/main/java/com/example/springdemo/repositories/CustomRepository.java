package com.example.springdemo.repositories;

import com.example.springdemo.entities.CustomMessage;
import com.example.springdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomRepository extends JpaRepository<CustomMessage, Integer> {

    User findById(int id);
    List<CustomMessage> findAll();

}
