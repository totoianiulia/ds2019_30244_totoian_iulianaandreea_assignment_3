package com.example.springdemo.repositories;

import com.example.springdemo.entities.Medicationn;
import com.example.springdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<Medicationn, Integer> {

    Medicationn findById(int id);
    List<Medicationn> findAll();
    Medicationn findByName(String name);

}