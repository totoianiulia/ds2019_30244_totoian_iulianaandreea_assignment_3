package com.example.springdemo.dto.builders;


import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Medicationn;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan person){
        return new MedicationPlanDTO(
                person.getUserId(),
                person.getStartTime(),
                person.getStopTime(),
                person.getMedications(),
                person.getPatient(),
                person.getUser());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO person){
        return new MedicationPlan(
                person.getUserId(),
                person.getStartTime(),
                person.getStopTime(),
                person.getMedications(),
                person.getPatient(),
                person.getUser());
    }
}
