package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserDTOForCreate;
import com.example.springdemo.entities.Person;
import com.example.springdemo.entities.User;


public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User person){
        return new UserDTO(
                person.getUserId(),
                person.getName(),
                person.getUsername(),
                person.getPassword(),
                person.getBirthdate(),
                person.getGender(),
                person.getAddress(),
                person.getItems());
    }

    public static User generateEntityFromDTO(UserDTO person){
        return new User(
              person.getUserId(),
                person.getName(),
                person.getUsername(),
                person.getPassword(),
                person.getBirthdate(),
                person.getGender(),
                person.getAddress(),
                person.getItems());
    }

    public static User generateEntity(UserDTOForCreate dto){
        User user  = new User();
        user.setAddress(dto.getAddress());
        user.setBirthdate(dto.getBirthdate());
        user.setGender(dto.getGender());
        user.setItems(dto.getItems());
        user.setName(dto.getName());
        user.setPassword(dto.getPassword());
        user.setUsername(dto.getUsername());
        return user;
    }
}
