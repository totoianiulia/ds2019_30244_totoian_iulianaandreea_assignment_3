package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.entities.Medicationn;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medicationn person){
        return new MedicationDTO(
                person.getMedId(),
                person.getName(),
                person.getSideEffects(),
                person.getDosage(),
                person.getMoment());
    }

    public static Medicationn generateEntityFromDTO(MedicationDTO person){
        return new Medicationn(
                person.getMedId(),
                person.getName(),
                person.getSideEffects(),
                person.getDosage(),
                person.getMoment());
    }

}
