package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.UserDTOForCreate;
import com.example.springdemo.entities.Caregiver;


public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver person){
        return new CaregiverDTO(
                person.getUserId(),
                person.getName(),
                person.getUsername(),
                person.getPassword(),
                person.getBirthdate(),
                person.getGender(),
                person.getAddress(),
                person.getPatients());
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO person){
        return new Caregiver(
                person.getUserId(),
                person.getName(),
                person.getUsername(),
                person.getPassword(),
                person.getBirthdate(),
                person.getGender(),
                person.getAddress(),
                person.getPatients());
    }
}
