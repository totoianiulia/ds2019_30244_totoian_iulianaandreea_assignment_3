package com.example.springdemo.dto.builders;


import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient person){
        return new PatientDTO(
                person.getPatientId(),
                person.getName(),
                person.getBirthdate(),
                person.getGender(),
                person.getAddress(),
                person.getMedicPlan(),
                person.getCaregiver());
    }

    public static Patient generateEntityFromDTO(PatientDTO person){
        return new Patient(
                person.getPatientId(),
                person.getName(),
                person.getBirthdate(),
                person.getGender(),
                person.getAddress(),
                person.getMedicPlan(),
                person.getCaregiver());

    }
}
