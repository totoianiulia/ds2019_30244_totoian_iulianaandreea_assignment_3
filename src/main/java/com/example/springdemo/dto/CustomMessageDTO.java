package com.example.springdemo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public final class CustomMessageDTO implements Serializable {

    private final String start_time;
    private final String end_time;
    private final String activity_label;

    public CustomMessageDTO(@JsonProperty("start_time") String start_time,
                            @JsonProperty("end_time") String end_time,
                            @JsonProperty("activity_label") String activity_label) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity_label = activity_label;
    }

    public String getStartTime() {
        return start_time;
    }

    public String getEndTime() {
        return end_time;
    }

    public String getActivity() {
        return activity_label;
    }

    @Override
    public String toString() {
        return "CustomMessage{" +
                "start_time='" + start_time +
                ", end_time=" + end_time +
                ", activity_label=" + activity_label +
                '}';
    }
}
