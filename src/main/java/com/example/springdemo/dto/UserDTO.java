package com.example.springdemo.dto;

import com.example.springdemo.entities.Item;
import com.example.springdemo.entities.MedicationPlan;


import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

public class UserDTO {

//
    private Integer userId;

    private String name;
    private String username;
    private String password;
    private Date birthdate;
    private String gender;
    private String address;
    private List<MedicationPlan> items;

    public UserDTO() {}

    public UserDTO(Integer userId, String name, String username, String password, Date birthdate, String gender, String address, List<MedicationPlan> items) {
        this.userId = userId;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.items = items;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<MedicationPlan> getItems() {
        return items;
    }

    public void setItems(List<MedicationPlan> items) {
        this.items = items;
    }
}
