package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.MedicationPlan;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;

public class PatientDTO {

    private int patientId;
    private String name;
    private Date birthdate;
    private String gender;
    private String address;

    private List<MedicationPlan> medicPlan;
    @JsonIgnore
    private Caregiver caregiver;

    public PatientDTO(int patientId, String name, Date birthdate, String gender, String address, List<MedicationPlan> medicPlan,Caregiver caregiver) {
        this.patientId = patientId;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicPlan = medicPlan;
        this.caregiver=caregiver;
    }

    public PatientDTO(String name, Date birthdate, String gender, String address, List<MedicationPlan> medicPlan,Caregiver caregiver) {

        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicPlan = medicPlan;
        this.caregiver=caregiver;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<MedicationPlan> getMedicPlan() {
        return medicPlan;
    }

    public void setMedicPlan(List<MedicationPlan> medicPlan) {
        this.medicPlan = medicPlan;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}
