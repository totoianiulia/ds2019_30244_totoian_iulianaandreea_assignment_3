package com.example.springdemo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public final class CustomMessage1 implements Serializable {

    private final String text;
    private final String priority;

    public CustomMessage1(@JsonProperty("text") String text,
                          @JsonProperty("priority") String priority) {

        this.text = text;
        this.priority = priority;
    }

    public String getText() {
        return text;
    }

    public String getPriority() {
        return priority;
    }



    @Override
    public String toString() {
        return "CustomMessage{" +
                "text='" + text + '\'' +
                ", priority=" + priority +
                ", secret=" +
                '}';
    }
}