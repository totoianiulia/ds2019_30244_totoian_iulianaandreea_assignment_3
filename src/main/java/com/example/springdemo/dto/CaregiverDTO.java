package com.example.springdemo.dto;

import com.example.springdemo.entities.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;

public class CaregiverDTO {


    private Integer caregiver_id;
    private String name;
    private String username;
    private String password;
    private Date birthdate;
    private String gender;
    private String address;
    @JsonIgnore
    private List<Patient> patients;

    public CaregiverDTO(Integer id, String name, String username, String password, Date birthdate, String gender, String address, List<Patient> patients) {
        this.caregiver_id=id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public Integer getUserId() {
        return caregiver_id;
    }

    public void setUserId(Integer userId) {
        this.caregiver_id = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
