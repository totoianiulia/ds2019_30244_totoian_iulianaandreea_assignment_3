package com.example.springdemo.dto;

import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Medicationn;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

public class MedicationPlanDTO {

    private Integer userId;
    private Date startTime;
    private Date stopTime;
    private List<Medicationn> medications;
    private Patient patient;
    private User user;

    public MedicationPlanDTO(Integer id, Date startTime, Date stopTime, List<Medicationn> medication, Patient user, User user2) {
        this.userId = id;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.medications = medication;
        this.patient = user;
        this.user = user2;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public List<Medicationn> getMedications() {
        return medications;
    }

    public void setMedications(List<Medicationn> medications) {
        this.medications = medications;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}




