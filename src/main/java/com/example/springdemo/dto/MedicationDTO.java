package com.example.springdemo.dto;

public class MedicationDTO {

    private Integer medId;
    private String name;
    private String sideEffects;
    private String dosage;
    private String moment;

    public MedicationDTO(Integer medId, String name, String sideEffects, String dosage,String moment) {
        this.medId = medId;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.moment=moment;
    }

    public MedicationDTO( String name, String sideEffects, String dosage,String moment) {

        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.moment=moment;
    }

    public Integer getMedId() {
        return medId;
    }

    public void setMedId(Integer medId) {
        this.medId = medId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

}
