package com.example.springdemo.controller;
import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    @Autowired
    private MedicationService medService;


    public MedicationController(MedicationService medication) {

        this.medService = medication;
    }


    @GetMapping()
    public List<MedicationDTO> findAll(){

        return medService.seeAll();
    }

    @PostMapping()
    public void insertPersonDTO(@RequestBody MedicationDTO user){

        medService.insertMedication(user);
    }

    @PutMapping()
    public int updateUser(@RequestBody MedicationDTO userDTO) {

        System.out.println("Am ajuns in UserController");
        return medService.updateMedication(userDTO);
    }


    @DeleteMapping("/{medId}")
    public void delete(@PathVariable Integer userId){

        medService.deleteMedication(userId);
    }
}


