package com.example.springdemo.controller;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;


    public PatientController(PatientService patient) {

        this.patientService = patient;
    } // vrei sa zici ca din nack imi returneaza uratenia aia? asa cred. di ciii?


    @GetMapping()
    public List<PatientDTO> findAll(){

        List<PatientDTO> lis= patientService.seeAll();
        return lis;
    }

    @PostMapping()
    public void insertPatient(@RequestBody PatientDTO patient){

        patientService.insertPacient(patient);
    }

    @PutMapping()
    public int updatePatient(@RequestBody PatientDTO patientDTO) {

        System.out.println("Am ajuns in PatientController");
        return patientService.updatePatient(patientDTO);
    }


    @DeleteMapping("/{patientId}")
    public void delete(@PathVariable Integer patientId){

        patientService.deletePatient(patientId);
    }
}


