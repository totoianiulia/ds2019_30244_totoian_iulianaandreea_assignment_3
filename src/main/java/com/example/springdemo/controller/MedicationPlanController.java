package com.example.springdemo.controller;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.services.MedicationPlanService;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class MedicationPlanController {

    @Autowired
    private MedicationPlanService medService;


    public MedicationPlanController(MedicationPlanService medication) {

        this.medService = medication;
    }


    @GetMapping()
    public List<MedicationPlanDTO> findAll(){

        List<MedicationPlanDTO> list = medService.seeAllMedicarionPlans();
        return list;
    }

    @PostMapping()
    public void insertPersonDTO(@RequestBody MedicationPlanDTO user){

        medService.insertMedicationPlan(user);
    }

    @PutMapping()
    public int updateUser(@RequestBody MedicationPlanDTO userDTO) {

        System.out.println("Am ajuns in UserController");
        //return medService.updateMedication(userDTO);
        return 0;
    }


    @DeleteMapping("/{medId}")
    public void delete(@PathVariable Integer userId){

        medService.deleteMedication(userId);
    }

}
