package com.example.springdemo.controller;
import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;


    public CaregiverController(CaregiverService caregiver) {

        this.caregiverService = caregiver;
    }


    @GetMapping()
    public List<CaregiverDTO> findAll(){

        System.out.println("am ajuns in CaregiverController");
        return caregiverService.seeAll();
    }

    @PostMapping()
    public void insertCaregiver(@RequestBody CaregiverDTO caregiver){

        caregiverService.insertCaregiver(caregiver);
    }

    @PutMapping()
    public int updateCaregiver(@RequestBody CaregiverDTO userDTO) {

        return caregiverService.updateCaregiver(userDTO);
    }


    @DeleteMapping("/{caregiver_id}")
    public void delete(@PathVariable Integer caregiver_id){

        System.out.println("------Pas 1 ----");
        caregiverService.deleteCaregiver(caregiver_id);
    }
}


