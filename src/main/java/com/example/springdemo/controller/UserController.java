package com.example.springdemo.controller;
import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserDTOForCreate;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;


    public UserController(UserService user) {

        this.userService = user;
    }


    @GetMapping()
    public List<UserDTO> findAll(){

        return userService.seeAll();
    }

    @PostMapping()
    public void insertPersonDTO(@RequestBody UserDTOForCreate user){

         userService.insertUser(user);
    }

   @PutMapping()
    public int updateUser(@RequestBody UserDTO userDTO) {

        System.out.println("Am ajuns in UserController");
        return userService.updateUser(userDTO);
    }


    @DeleteMapping("/{userId}")
    public void delete(@PathVariable Integer userId){

        userService.deleteUser(userId);
    }
}


