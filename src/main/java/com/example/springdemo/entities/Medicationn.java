package com.example.springdemo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")


public class Medicationn {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medId", unique = true, nullable = false)
    private Integer medId;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "sideEffects",  length = 200)
    private String sideEffects;

    @Column(name = "dosage",  length = 200)
    private String dosage;

    @Column(name = "moment",  length = 200)
    private String moment;

    @ManyToOne()
    @JoinColumn(name = "medPlanId",  nullable = false)
    private MedicationPlan medicationPlan;

    public Medicationn(){}

    public Medicationn(int id, String name, String sideEffects, String dosage, String moment) {
        this.medId=id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.moment=moment;
    }

    public int getMedId() {
        return medId.intValue();
    }

    public void setMedId(Integer medId) {
        this.medId = medId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }
}
