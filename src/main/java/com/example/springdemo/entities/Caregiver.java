package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.Date;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "caregiver")
public class Caregiver {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "caregiver_id")
    private Integer caregiver_id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "username", nullable = false, unique=true, length = 200)
    private String username;

    @Column(name = "password", nullable = false, length = 200)
    private String password;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients;

    public Caregiver(){}

    public Caregiver(String name, String username, String password, Date birthdate, String gender, String address, List<Patient> patients) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public Caregiver(Integer id, String name, String username, String password, Date birthdate, String gender, String address, List<Patient> patients) {
        this.caregiver_id=id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public Integer getUserId() {
        return caregiver_id;
    }

    public void setUserId(Integer userId) {
        this.caregiver_id = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
