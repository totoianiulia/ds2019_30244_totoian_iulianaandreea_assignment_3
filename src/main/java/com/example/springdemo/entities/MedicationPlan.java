package com.example.springdemo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medicationPlan")
@NoArgsConstructor
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medPlanId", unique = true, nullable = false)
    private Integer medPlanId;

    @Column(name = "startTime", nullable = false, length = 100)
    private Date startTime;

    @Column(name = "stopTime", nullable = false, length = 100)
    private Date stopTime;

    @OneToMany(mappedBy = "medicationPlan",
            fetch = FetchType.EAGER)
    private List<Medicationn> medications;

    @ManyToOne()
    @JoinColumn(name = "patientId",  nullable = false)
    private Patient patient;

    @ManyToOne()
    @JoinColumn(name = "userId",  nullable = false)
    private User user;

    public MedicationPlan(Integer id,Date startTime, Date stopTime, List<Medicationn> medication,
                          Patient user,User user2) {
        this.medPlanId=id;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.medications=medication;
        this.patient = user;
        this.user=user2;
    }

    public Integer getUserId() {
        return medPlanId;
    }

    public void setUserId(Integer userId) {
        this.medPlanId = userId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(Patient user) {
        this.patient = user;
    }

    public List<Medicationn> getMedications() {
        return medications;
    }

    public void setMedications(List<Medicationn> medications) {
        this.medications = medications;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
