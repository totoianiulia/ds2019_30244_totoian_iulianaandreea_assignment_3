package com.example.springdemo.entities;

import org.springframework.web.bind.annotation.CrossOrigin;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "item")


public class Item {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100, unique=true, nullable = false)
    private String name;

    @ManyToOne()
    @JoinColumn(name = "personId",  nullable = false)
    private Person person;

    public Item() {
    }

    public Item(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
/*
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;*/

}
