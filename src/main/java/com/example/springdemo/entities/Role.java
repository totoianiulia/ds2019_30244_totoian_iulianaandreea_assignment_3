package com.example.springdemo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "role")


public class Role {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "roleId", unique = true, nullable = false)
    private Integer userId;

    @Column(name = "type")
    private Type type;
}
