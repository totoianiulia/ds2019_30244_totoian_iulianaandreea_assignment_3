package com.example.springdemo.entities;

public enum IntakeMoment {
    morning, lunch, dinner, bedtime
}
