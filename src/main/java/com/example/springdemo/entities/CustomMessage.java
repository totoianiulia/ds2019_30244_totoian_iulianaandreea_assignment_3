package com.example.springdemo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;
@Entity
public final class CustomMessage {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "custom_id")
    private int id;

    @Column(name = "start_time", nullable = false, length = 100)
    private  String start_time;

    @Column(name = "end_time", nullable = false, length = 100)
    private  String end_time;

    @Column(name = "activity_laber", nullable = false, length = 100)
    private  String activity_laber;

    @Column(name = "problems")
    private boolean problems;

    public CustomMessage(String s1, String s2, String s3, boolean b)
        {this.start_time = s1;
        this.end_time = s2;
        this.activity_laber = s3;
        this.problems=b;
    }

    public String getStartTime() {
        return start_time;
    }

    public String getEndTime() {
        return end_time;
    }

    public String getActivity() {
        return activity_laber;
    }

    public void setProblems(boolean pb){this.problems=pb;}

    @Override
    public String toString() {
        return "CustomMessage{" +
                "text='" + start_time +
                ", priority=" + end_time +
                ", secret=" + activity_laber +
                '}';
    }
}