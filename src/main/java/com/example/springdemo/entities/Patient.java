package com.example.springdemo.entities;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")


public class Patient {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "patientId", unique = true, nullable = false)
    private int patientId;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

     @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
    private List<MedicationPlan> medicPlan;

    @ManyToOne()
    @JoinColumn(name = "caregiver_id",  nullable = false)
    private Caregiver caregiver;

    public Patient(){}

    public Patient(int id, String name, Date birthdate, String gender, String address, List<MedicationPlan> medicPlan, Caregiver caregiver) {
        this.patientId=id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicPlan = medicPlan;
        this.caregiver=caregiver;
    }

    public Patient(String name, Date birthdate, String gender, String address, List<MedicationPlan> medicPlan, Caregiver caregiver) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicPlan = medicPlan;
        this.caregiver = caregiver;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<MedicationPlan> getMedicPlan() {
        return medicPlan;
    }

    public void setMedicPlan(List<MedicationPlan> medicPlan) {
        this.medicPlan = medicPlan;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}
