package com.example.springdemo.services;


import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.UserDTOForCreate;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicationRepository medRepository;

    public List<CaregiverDTO> seeAll() {
        System.out.println("CaregiverService-afis");
        List<Caregiver> caregivers = caregiverRepository.findAll();
        System.out.println("lista are: " + caregivers.size());
        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public void insertCaregiver(CaregiverDTO caregiverDTO) {

        Caregiver caregiver = caregiverRepository.findByUsername(caregiverDTO.getUsername());
        if (caregiver != null) {
            throw new DuplicateEntryException("Caregiver", "username", caregiverDTO.getUsername());
        }

        Caregiver caregiver1 = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        caregiver1.setUserId(100);

        caregiverRepository.save(caregiver1);
    }

    public int updateCaregiver(CaregiverDTO userDTO) {

        Caregiver user = caregiverRepository.findByUsername(userDTO.getUsername()
        );
        if (user == null) {
            throw new ResourceNotFoundException("Caregiver", "userId", userDTO.getUserId());
        }

        user.setAddress(userDTO.getAddress());
        user.setBirthdate(userDTO.getBirthdate());
        user.setGender(userDTO.getGender());
        user.setPatients(userDTO.getPatients());
        user.setName(userDTO.getName());
        user.setPassword(userDTO.getPassword());
        user.setUsername(userDTO.getUsername());

        return caregiverRepository
                .save(user)
                .getUserId();
    }



    public void deleteCaregiver(Integer idul) {

        System.out.println("------Pas 2 ----");
        this.caregiverRepository.deleteById(idul);
    }



}
