package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.dto.builders.MedicationPlanBuilder;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.repositories.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    @Autowired
    public MedicationPlanService(MedicationPlanRepository medRepository) {
        this.medRepository = medRepository;
    }


    private MedicationPlanRepository medRepository;

    public List<MedicationPlanDTO> seeAllMedicarionPlans() {
        List<MedicationPlan> medications = medRepository.findAll();
        System.out.println("lista are: " + medications.size());


        return medications.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public  List<MedicationPlan> getMedicPlan()
    {
        return medRepository.findAll();
    }


    public int insertMedicationPlan(MedicationPlanDTO medicationDTO) {

        MedicationPlan medication = medRepository.findById(medicationDTO.getUserId());
        if(medication != null){
            throw  new DuplicateEntryException("MedicationPlan", "id", medicationDTO.getUserId());
        }

        return medRepository
                .save(MedicationPlanBuilder.generateEntityFromDTO(medicationDTO))
                .getUserId();
    }

   /* public int updateMedicationPlan(MedicationPlanDTO medicationDTO) {

        MedicationPlan user = medRepository.findById(medicationDTO.getUserId());
        if (user == null) {
            throw new ResourceNotFoundException("MedicatinPlan", "userId", medicationDTO.getUserId());
        }

        user.setDosage(medicationDTO.getDosage());
        user.setName(medicationDTO.getName());
        user.setSideEffects(medicationDTO.getSideEffects());

        return medRepository
                .save(user)
                .getMedId();
    }*/

    public void deleteMedication(Integer idul) {

        this.medRepository.deleteById(idul);
    }



}
