package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplication;
import com.example.springdemo.dto.CustomMessage1;
import com.example.springdemo.dto.CustomMessageDTO;
import com.example.springdemo.entities.CustomMessage;
import com.example.springdemo.repositories.CustomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class CustomMessageListener1 {

    private static final Logger log = LoggerFactory.getLogger(CustomMessageListener.class);

    @Autowired
    CustomRepository customRepository;


    @RabbitListener(queues = SpringDemoApplication.QUEUE_SPECIFIC_NAME)
    public void receiveMessage(final CustomMessage1 customMessage)throws Exception {
        log.info("Received message as specific class: {}", customMessage.toString());
        verifAndSaveMessage(customMessage);
    }

    public void verifAndSaveMessage(CustomMessage1 customMessage) throws Exception
    {

        CustomMessage message;
        String sp1=customMessage.getText();  //impart start time si end time
        String[] sp2=sp1.split("\t\t",2);



        Date dateStart= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sp2[0]);
        Date dateEnd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sp2[1]);
        long comp12 = new Long(12);
        long comp1 = new Long(0);

        System.out.println(dateStart);
        System.out.println(dateEnd);

        long e=dateEnd.getTime();
        long s=dateStart.getTime();
        long difer= e-s;
        System.out.println(dateEnd.getTime());
        System.out.println(dateStart.getTime());
        System.out.println(difer);
        long differenceInHours= TimeUnit.MILLISECONDS.toHours(difer);
        System.out.println(differenceInHours);

        if(customMessage.getPriority().contains("Sleeping") &&
                differenceInHours>comp12 )
        {

            message= new CustomMessage(sp2[0],sp2[1],customMessage.getPriority(),true);
        }
        else {

            //R2

            if (customMessage.getPriority().contains("Leaving") &&
                    (differenceInHours > comp12)) {
                message = new CustomMessage(sp2[0], sp2[1], customMessage.getPriority(), true);
            } else {

                //R3
                if (customMessage.getPriority().contains("Toileting") &&
                        (differenceInHours) > comp1) {
                    message = new CustomMessage(sp2[0], sp2[1], customMessage.getPriority(), true);
                } else message = new CustomMessage(sp2[0], sp2[1], customMessage.getPriority(), false);
            }
        }
        customRepository.save(message);

    }




}
