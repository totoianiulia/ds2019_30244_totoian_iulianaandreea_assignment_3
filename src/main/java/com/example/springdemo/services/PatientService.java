package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientService {



    @Autowired
    private PatientRepository patientRepository;

    public List<PatientDTO> seeAll() {
        List<Patient> users = patientRepository.findAll();
        System.out.println("lista are: " + users.size());
        return users.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public int insertPacient(PatientDTO pacientDTO) {
        Patient patient = patientRepository.findById(pacientDTO.getPatientId());
        if (patient != null) {
            throw
                    new DuplicateEntryException("Patient", "patientId", pacientDTO.getPatientId());
        }
        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(pacientDTO))
                .getPatientId();
    }


    public int updatePatient(PatientDTO patientDTO) {

        Patient patient = patientRepository.findById(patientDTO.getPatientId());

        if (patient != null) {
            throw new ResourceNotFoundException("Patient", "patientId", patientDTO.getPatientId());
        }


        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getPatientId();
    }


    public void deletePatient(Integer patientId) {

        this.patientRepository.deleteById(patientId);
    }


}
