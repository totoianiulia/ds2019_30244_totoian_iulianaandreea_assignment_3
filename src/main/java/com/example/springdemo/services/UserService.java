package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserDTOForCreate;
import com.example.springdemo.dto.builders.MedicationBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.UserBuilder;
import com.example.springdemo.entities.Medicationn;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicationRepository medRepository;

    public List<UserDTO> seeAll() {
        List<User> users = userRepository.findAll();
        System.out.println("lista are: " + users.size());
        return users.stream()
                .map(UserBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public void insertUser(UserDTOForCreate userDTO) {

        User user = userRepository.findByUsername(userDTO.getUsername());
        if (user != null) {
            throw new DuplicateEntryException("User", "username", userDTO.getUsername());
        }

        User userD = UserBuilder.generateEntity(userDTO);
        userD.setUserId(100);

        userRepository.save(userD);
    }

    public int insertPacient(PatientDTO pacientDTO) {
        Patient patient = patientRepository.findById(pacientDTO.getPatientId());
        if (patient != null) {
            throw
                    new DuplicateEntryException("Patient", "id", pacientDTO.getPatientId());
        }
        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(pacientDTO))
                .getPatientId();
    }

    /*public int insertMedication(MedicationDTO medicationDTO) {

        Medicationn medication = medRepository.findById(medicationDTO.getMedId());
        if(medication != null){
            throw  new DuplicateEntryException("Medication", "id", medicationDTO.getMedId());
        }

        return medRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getMedId();
    }*/

    public int updateUser(UserDTO userDTO) {

        User user = userRepository.findByUsername(userDTO.getUsername()
        );
       if (user == null) {
            throw new ResourceNotFoundException("User", "userId", userDTO.getUserId());
        }

        user.setAddress(userDTO.getAddress());
        user.setBirthdate(userDTO.getBirthdate());
        user.setGender(userDTO.getGender());
        user.setItems(userDTO.getItems());
        user.setName(userDTO.getName());
        user.setPassword(userDTO.getPassword());
        user.setUsername(userDTO.getUsername());

        return userRepository
                .save(user)
                 .getUserId();
    }

    public int updatePatient(PatientDTO patientDTO) {

        Patient patient = patientRepository.findById(patientDTO.getPatientId());

        if (patient != null) {
            throw new ResourceNotFoundException("Patient", "patientId", patientDTO.getPatientId());
        }


        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getPatientId();
    }

    public void deleteUser(Integer idul) {

        this.userRepository.deleteById(idul);
    }

    public void deletePatient(PatientDTO patientDTO) {

        this.patientRepository.deleteById(patientDTO.getPatientId());
    }


}
