package com.example.springdemo.services;

import com.codenotfound.grpc.helloworld.Empty;
import com.codenotfound.grpc.helloworld.HelloWorldServiceGrpc;
import com.codenotfound.grpc.helloworld.ListOfMedication;
import com.example.springdemo.entities.*;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@GRpcService
public class HelloWorldServiceImpl
    extends HelloWorldServiceGrpc.HelloWorldServiceImplBase {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(HelloWorldServiceImpl.class);

    @Autowired
    MedicationPlanService medService;


   // @Scheduled
    @Override
    public void sayHello(Empty request,
                         StreamObserver<ListOfMedication> responseObserver) {
        LOGGER.info("server received {}");

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        String dimi="";
        String amiaza="";
        String seara="";
        String pacient="";

       List<MedicationPlan> list=medService.getMedicPlan();


        for(MedicationPlan m: list)
        {
            LocalDateTime start= m.getStartTime().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
            LocalDateTime stop= m.getStopTime().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();

            if(now.compareTo(start)>0 && now.compareTo(stop)<0){

                List<Medicationn> list2 = m.getMedications();
                pacient= m.getPatient().getName();
                for(Medicationn m2: list2)
                {
                    String moment=m2.getMoment();
                    if(moment.equals("dimi"))
                    {
                        String newItem=m2.getName()+",";
                        dimi=dimi+newItem;
                    }

                    if(moment.equals("amiaza"))
                    {
                        String newItem=m2.getName()+",";
                        amiaza=amiaza+newItem;
                    }

                    if(moment.equals("seara"))
                    {
                        String newItem=m2.getName()+",";
                        seara=seara+newItem;
                    }
                }
            }

        }

        System.out.println("DIM: "+dimi);
        System.out.println("AMIAZA: "+ amiaza);
        System.out.println("SEARA: "+seara);

        ListOfMedication greeting =
                ListOfMedication.newBuilder().setMesaj1(dimi).setMesaj2(amiaza)
                        .setMesaj3(seara).setMesaj4(pacient).build();
        LOGGER.info("server responded {}", greeting);

        responseObserver.onNext(greeting);
        responseObserver.onCompleted();
    }



}