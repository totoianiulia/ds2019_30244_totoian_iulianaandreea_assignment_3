package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.builders.MedicationBuilder;
import com.example.springdemo.entities.Medicationn;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationService {



    @Autowired
    private MedicationRepository medRepository;

    public List<MedicationDTO> seeAll() {
        List<Medicationn> medications = medRepository.findAll();
        System.out.println("lista are: " + medications.size());
        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    /*public void insertUser(UserDTOForCreate userDTO) {

        User user = userRepository.findByUsername(userDTO.getUsername());
        if (user != null) {
            throw new DuplicateEntryException("User", "username", userDTO.getUsername());
        }

        User userD = UserBuilder.generateEntity(userDTO);
        userD.setUserId(100);

        userRepository.save(userD);
    }*/

    public int insertMedication(MedicationDTO medicationDTO) {

        Medicationn medication = medRepository.findByName(medicationDTO.getName());
        if(medication != null){
            throw  new DuplicateEntryException("Medication", "id", medicationDTO.getMedId());
        }

        return medRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getMedId();
    }

    public int updateMedication(MedicationDTO userDTO) {

        Medicationn user = medRepository.findByName(userDTO.getName());
        if (user == null) {
            throw new ResourceNotFoundException("User", "userId", userDTO.getMedId());
        }

        user.setDosage(userDTO.getDosage());
        user.setName(userDTO.getName());
        user.setSideEffects(userDTO.getSideEffects());

        return medRepository
                .save(user)
                .getMedId();
    }

    public void deleteMedication(Integer idul) {

        this.medRepository.deleteById(idul);
    }




}
