package com.codenotfound;


import com.codenotfound.grpc.HelloWorldClient;

public class SpringGrpcApplication {

    public static void main(String[] args) {

        HelloWorldClient helloWorldClient = new HelloWorldClient();
        helloWorldClient.sayHello("Iulia", "Totoian");

    }
}
