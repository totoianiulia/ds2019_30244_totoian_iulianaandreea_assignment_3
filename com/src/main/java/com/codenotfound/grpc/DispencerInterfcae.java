package com.codenotfound.grpc;

import org.apache.log4j.lf5.viewer.LogTableRowRenderer;

import javax.swing.JTable;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DispencerInterfcae extends JFrame {


    JPanel panel1;
    JPanel panel2;
    JPanel panel3;
    JLabel label;
    JLabel labelMoment;
    JButton button;
    JTable table;
    Object[][] data;//={{"ana"},{"are"}};
    DefaultTableModel model;
    String messToServer;

    public DispencerInterfcae() {
        this.setTitle("Dispancer Application");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 300);

        panel1 = new JPanel(new BorderLayout()); // the panel is not visible in output
        panel2 = new JPanel();
        panel3 = new JPanel();
        label = new JLabel("Lista medicamentelor care trebuie luate");
        button = new JButton("Taken");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                // check for selected row first
                if (table.getSelectedRow() != -1) {
                    // remove selected row from the model
                    model.removeRow(table.getSelectedRow());
                    JOptionPane.showMessageDialog(null, "Selected medication was taken!");
                    setMessToServer("Medicamentul a fost luat");

                }
            }
        });



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        final JLabel timeLabel = new JLabel();
        add(timeLabel);

        final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        ActionListener timerListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Date date = new Date();
                String time = timeFormat.format(date);
                timeLabel.setText(time);
            }
        };
        Timer timer = new Timer(1000, timerListener);
        // to make sure it doesn't wait one second at the start
        timer.setInitialDelay(0);
        timer.start();

        ////////////////////////////////////////////////////////////////////////////////////////////////////


        panel1.add(timeLabel, BorderLayout.CENTER);
        panel1.add(label, BorderLayout.AFTER_LAST_LINE);
//      panel1.add(labelMoment);
        panel2.add(button);
        //panel3.add(table);
        this.getContentPane().add(BorderLayout.NORTH, panel1);
        this.getContentPane().add(BorderLayout.SOUTH, panel2);
        this.getContentPane().add(BorderLayout.CENTER, panel3);
        this.setVisible(true);
    }

    public void setVals(String[][] vals) {

        this.data = vals;
        Object[] columnsName = {"medicamente"};
        //model = new DefaultTableModel(data, columnsName);

        table = new JTable(data, columnsName);
        table.setBounds(30, 40, 100, 100);
        table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        panel3.add(table, BorderLayout.AFTER_LAST_LINE);
        this.getContentPane().add(BorderLayout.CENTER, panel3);
        this.setVisible(true);
    }



    public void setLabelMoment(String val) {

        labelMoment = new JLabel(val);
        System.out.println("aici " + val);
        panel3.add(labelMoment, BorderLayout.NORTH);
    }

    public void setMessToServer(String ms) {
        this.messToServer = ms;
    }

    public String getMessToServer() {
        return this.messToServer;
    }

}
