
import com.codenotfound.grpc.HelloWorldClient;
import javafx.application.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;



@EnableScheduling
public class Main {



    public static void main(String[] args) throws SQLException {

        HelloWorldClient helloWorldClient = new HelloWorldClient();
        helloWorldClient.sayHello();

        //DispencerInterfcae dispencerInterfcae=new DispencerInterfcae();

        AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
               // DispencerInterfcae dispencerInterfcae=new DispencerInterfcae();

            }
        });

    }
}